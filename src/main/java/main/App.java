package main;

import static spark.Spark.get;
public class App {
    public static void main(String[] args) {
        get("/hello",(req,res)->"Hello World");
//        get("/hello/:name",(req,res)->"Hello, "+req.params("name"));
        get("/hello/:nama/:kelas",(req,res)->{
           String nama = req.params("nama");
           String kelas = req.params("kelas");

           return "halo, "+nama+"<br> Kelas "+kelas;
        });

        get("/hello/:nilai1/operator/:nilai2",(req,res)->{
            String operator = req.params("operator");
            String nilai1 = req.params("nilai1");
            String nilai2 = req.params("nilai2");

            int input1 = Integer.parseInt(nilai1);
            int input2 = Integer.parseInt(nilai2);
            int hasil;

            if(operator.equalsIgnoreCase("tambah")){
                hasil = input1+input2;
                return hasil;
            }else if (operator.equalsIgnoreCase("kurang")){
                hasil = input1-input2;
                return "adalah "+hasil;
            }else if(operator.equalsIgnoreCase("bagi")){
                hasil = input1/input2;
                return hasil;
            }else if(operator.equalsIgnoreCase("kali")){
                hasil = input1*input2;
                return hasil;
            }else {
                return "operator harus perkata";
            }

        });
    }
}
